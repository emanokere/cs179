def introScene():
     directions = ["kitchen", "dining", "upstairs", "leave"]
     userInput = input("You are in the old home and feel an eerie chill run down your spine. 'Where should I inspect?' Options: kitchen, dining, upstairs, leave\n")
     if userInput in directions:
        if userInput == "kitchen": 
             enterKitchen()
        elif userInput == "dining":
             enterDining()
        elif userInput == "upstairs":
             enterUpstairs()
        elif userInput == "leave":
             quit()
     while userInput not in directions:
        userInput = input("Options: kitchen / dining / upstairs / leave \n")
        if userInput == "kitchen": 
             enterKitchen()
        elif userInput == "dining":
             enterDining()
        elif userInput == "upstairs":
             enterUpstairs()
        elif userInput == "leave":
             quit()

                        
def enterKitchen():
     directions = ["floor", "dining", "back"]
     userInput = input("You enter the kitchen and smell something awful. You also see messy splatters on the floor. 'Where should I inspect?' Options: floor / dining / back\n")
     if userInput in directions:
        if userInput == "floor": 
             enterFloor()
        elif userInput == "dining":
             enterDining()
        elif userInput == "back":                
             introScene()     
     while userInput not in directions: 
        userInput = input("Options: floor / dining / back\n")
        if userInput == "floor": 
             enterFloor()
        elif userInput == "dining":
             enterDining()
        elif userInput == "back":                
             introScene()

def enterFloor():
    directions = ["dining", "upstairs",]
    userInput = input("You take a closer look at the splatters and its fresh blood... the trail is leading into the dining room. Options: dining / upstairs\n")
    if userInput in directions: 
        if userInput == "dining":
             enterDining()
        elif userInput == "upstairs":
             enterUpstairs()   
    while userInput not in directions:
        userInput = input("Options: dining, upstairs\n") 
        if userInput == "dining":
             enterDining()
        elif userInput == "upstairs":
             enterUpstairs()

def enterDining():
    directions = ["rug", "upstairs"]
    userInput = input("You enter the dining room and find a messy splatter trail leading to something under the rug. Options: rug / upstairs\n")
    if userInput in directions:
         if userInput == "rug": 
             enterRug()
         elif userInput == "upstairs":
             enterUpstairs()
    while userInput not in directions:
         userInput = input("Options: rug / upstairs")
         if userInput == "rug": 
             enterRug()
         elif userInput == "upstairs":
             enterUpstairs()

def enterRug():
    directions = ["pull", "upstairs", "kitchen", "leave"]
    userInput = input("You lift up the rug and find a locked cellar door... Options: pull / upstairs / kitchen / leave\n")
    if userInput in directions:
        if userInput == "pull":
             enterPull()
        elif userInput == "upstairs":
             enterUpstairs()
        elif userInput == "kitchen":
             enterKitchen()
        elif userInput == "leave":
             quit()
    while userInput not in directions: 
        userInput = input("Options: pull / upstairs / kitchen / leave")
        if userInput == "pull":
             enterPull()
        elif userInput == "upstairs":
             enterUpstairs()
        elif userInput == "kitchen":
             enterKitchen()
        elif userInput == "leave":
             quit()

def enterPull():
    directions = ["upstairs", "kitchen", "leave"] 
    userInput = input("You tug at the door handle but it won't budge. It needs a key to unlock. Options: upstairs / kitchen / leave\n")
    if userInput in directions: 
        if userInput == "upstairs":
             enterUpstairs()
        elif userInput == "kitchen":
             enterKitchen()
        elif userInput == "leave":
             quit()     
    while userInput not in directions:
        userInput = input("Options: upstairs / kitchen / leave")
        if userInput == "upstairs":
             enterUpstairs()
        elif userInput == "kitchen":
             enterKitchen()
        elif userInput == "leave":
             quit()    

def enterUpstairs():
    directions = ["drawer", "mattress", "dining", "kitchen", "leave"]        
    userInput = input("You walk upstairs and find the door open to the bedroom. Inside you see a broken drawer and a torn up mattress. Options: drawer / mattress / dining / kitchen / leave\n")  
    if userInput in directions:
        if userInput == "drawer":
             enterDrawer()
        elif userInput == "mattress":
             enterMattress()
        elif userInput == "dining":
             enterDining2()
        elif userInput == "kitchen":
             enterKitchen()
        elif userInput == "leave":
             quit() 
    while userInput not in directions:
        userInput = input("Options: drawer / photos / mattress / dining / kitchen / leave")
        if userInput == "drawer":
             enterDrawer()
        elif userInput == "mattress":
             enterMattress()
        elif userInput == "dining":
             enterDining2()
        elif userInput == "kitchen":
             enterKitchen()
        elif userInput == "leave":
             quit()

def enterDrawer():
    directions = ["mattress", "dining", "kitchen", "leave"]  
    userInput = input("You open the drawer and find a letter that reads 'I'll come back for you, I promise' Options: mattress / dining / kitchen / leave\n")
    if userInput in directions:
        if userInput == "mattress":
             enterMattress()
        elif userInput == "dining":
             enterDining2()
        elif userInput == "kitchen":
             enterKitchen()
        elif userInput == "leave":
             quit() 
    while userInput not in directions:
        userInput == input("Options: photos / mattress / dining / kitchen / leave")
        if userInput == "mattress":
             enterMattress()
        elif userInput == "dining":
             enterDining2()
        elif userInput == "kitchen":
             enterKitchen()
        elif userInput == "leave":
             quit()

def enterMattress():
    directions = ["drawer", "dining", "kitchen", "leave"] 
    userInput = input("You inspect the torn mattress and find a silver key hidden within it. 'This might be useful...' Options: drawer / dining / kitchen / leave\n")
    if userInput in directions: 
        if userInput == "drawer":
             enterDrawer()    
        elif userInput == "dining":
             enterDining2()
        elif userInput == "kitchen":
             enterKitchen()
        elif userInput == "leave":
             quit()
    while userInput not in directions:
        if userInput == "drawer":
             enterDrawer()    
        elif userInput == "dining":
             enterDining2()
        elif userInput == "kitchen":
             enterKitchen()
        elif userInput == "leave":
             quit()

def enterDining2():
    directions = ["unlock"] 
    userInput = input("You go downstairs to the dining room but suddenly hear pounding on the cellar door from the other side. Options: unlock\n") 
    if userInput in directions:
        if userInput == "unlock":
             enterUnlock()               
    while userInput not in directions:
        if userInput == "unlock":
             enterUnlock()


def enterUnlock():
    directions = ["run"]
    userInput = input("You unlock the cellar door and open slowly to reveal a horrid, green monstor with eyes bulging from it's head. It's gaze fixed upon you. It's patient glare is accompanied by the slow hiss it makes as it opens it's mouth wider and wider. Options: run\n")
    if userInput in directions: 
        if userInput == "run":
             enterRun()       
    while userInput not in directions:
        if userInput == "run":
             enterRun()

def enterRun():
    directions = ["goodbye"]
    userInput = input("Every muscle in your body has frozen over and you are unable to escape it's jarring, jagged teeth. Say goodbye Option: goodbye\n")
    if userInput in directions: 
        if userInput == "goodbye":
             quit()
    while userInput not in directions:
        if userInput == "goodbye":
             quit()

     
if __name__ == "__main__":
    print("'Ahh... Another beautiful day to find a property to rebuild.'")
    print("'Today I have a special project I'll be going to see.'")
    print("'The previous owners moved out in a hurry, so it was a steal!'")
    introScene() 
