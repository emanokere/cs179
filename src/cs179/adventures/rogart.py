answer = input("Would you like to go on a hike?")

if answer.lower().strip() == "yes":
    print("\nWonderful! You start your journey going up a tall and windy mountain.")
    print("All of a sudden, a hungry grizzly bear appears and starts racing towards you!")
    answer = input("Do you attempt to climb a [tree] or tumble down a hill towards a [lake] in the distance?")
    if answer == "lake":
        print("\nYou run away as fast as you can and barely escape the enraged grizzly.")
        print("You walk on and eventually stumble upon a small cabin right beside a waterfall.")
        print("\nNight is fast approaching and you need a place to sleep for the night.")
        answer = input("Do you search the [cabin] or do you choose to investigate the [waterfall]?")
        if answer == "cabin":
            print("\nYou knock on the door and find that the door was unlocked.")
            print("You let yourself in and see a group of stone-trolls that (lucky for you) are sleeping.")
            answer = input("Do you go [awaken] the trolls to ask for help or secretly [search] the cabin?")
            if answer == "awaken":
                print('''\nYou make your way over to the trolls and they abruptly awaken and ask, "Who are you?" ''')
                print("You explain your situation and the trolls understand as the bear has attacked them too.")
                print("\nThe trolls pack your bag with more supplies and helps you find the trail once again.")
                answer = input("Do you [continue] and finish the trail or [leave] before to avoid more danger?")
                if answer == "continue":
                    print("\nYou go along and finish the hike with no more danger to be seen.")
                    print("You later realize the trolls even gifted you an emerald charm to remember them by.")
                    print("Even after all the danger, you smile knowing the hike was still very much worth it.")
                    quit()
                elif answer == "leave":
                    print("\n You try to leave the trail but unknown to you, the bear was waiting for you.")
                    print("Though you escaped the bear once, you weren't so fortunate this time, please try again.")
                    quit()
                else:
                    print("Going off trail is not allowed, please follow the appropriate trailhead signs.")
                    quit()
            elif answer == "search":
                print("\nYou search the cabin but find nothing to contact or lead you to the outside world.")
                print("Suddenly, you turn around and the trolls are all staring at you, furious at an intruder.")
                print("You should have been a more honest guest, please try again.")
                quit()
            else:
                print("Going off trail is not allowed, please follow the appropriate trailhead signs.")
                quit()
        elif answer == "waterfall":
            print("\nYou investigate the waterfall and discover behind it a perfect place to sleep, a hidden cave.")
            print("You enter the cave and are greeted by a monstrous Yeti.")
            answer = input("Do you run [deeper] into the cave or do you run [outside] in hopes to escape?")
            if answer == "deeper":
                print("You attempt to go further into the cave but it's so dark that you trip and fall.")
                print("The yeti easily finds you, ending your adventure, please try again.")
                quit()
            elif answer == "outside":
                print("\nYou rush outside and lucky for you, the yeti does not think you are worth its time.")
                print("With nowhere else to go, you must approach the cabin if you want to ever go home again.")
                print("\nYou knock on the door and find that the door was unlocked.")
                print("You let yourself in and see a group of stone-trolls that (lucky for you) are sleeping.")
                answer = input("Do you go [awaken] the trolls to ask for help or secretly [search] the cabin?")
                if answer == "awaken":
                    print('''\nYou approach the trolls and they abruptly awaken and ask, "Who are you?" ''')
                    print("You explain your situation and the trolls understand as the bear has attacked them too.")
                    print("The trolls pack your bag with more supplies and helps you find the trail once again.")
                    answer = input("Do you [continue] and finish the trail or [leave] before to avoid more danger?")
                    if answer == "continue":
                        print("\nYou go along and finish the hike with no more danger to be seen.")
                        print("You later realize the trolls even gifted you an emerald charm to remember them by.")
                        print("Even after all the danger, you smile knowing the hike was still very much worth it.")
                        quit()
                    elif answer == "leave":
                        print("\n You try to leave the trail but unknown to you, the bear was waiting for you.")
                        print("Though you escaped the bear once, you weren't so fortunate this time, please try again.")
                        quit()
                    else:
                        print("Going off trail is not allowed, please follow the appropriate trailhead signs.")
                        quit()
                elif answer == "search":
                    print("\nYou search the cabin but find nothing to contact or lead you to the outside world.")
                    print("Suddenly, you turn around and the trolls are all staring at you, furious at an intruder.")
                    print("You should have been a more honest guest, please try again.")
                    quit()
                else:
                    print("Going off trail is not allowed, please follow the appropriate trailhead signs.")
                    quit()
            else:
                print("Going off trail is not allowed, please follow the appropriate trailhead signs.")
                quit()
        else:
            print("Going off trail is not allowed, please follow the appropriate trailhead signs.")
            quit()

    elif answer == "tree":
        print("Unfortunately, bears are talented tree climbers and you become its lunch, please try again.")
        quit()
    else:
        print("Going off trail is not allowed, please follow the appropriate trailhead signs.")
        quit()

elif "no":
    print("Come back when you're ready!")
    quit()