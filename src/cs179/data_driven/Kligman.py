#INTRO---------------------------------------------------------------------------------------------

print("You've been sent to a small town on a mission. You've been hired\n"
      "to assassinate one of the townsfolk! Your target was described to\n"
      "you as a 'burly man\'. Your employer has given you money for supplies.\n\n")

print("You have many options available to you! Explore the town and\n"
      "decide your course of action. When selecting an option, type 1, 2, 3 or 4.\n"
      "There are 6 special endings.\n\n")

#TEXT----------------------------------------------------------------------------------------------
def readDescription(name):

    reading = False
    description = ""
    rStart = ("start" + name)
    rEnd = ("end" + name)
    file = open("Kligman.txt", "r")
    lines = file.readlines()

    for line in lines:
        if(rEnd in line):
            reading = False
        if(reading):
            description += line
        if(rStart in line):
            reading = True
            
    file.close()
    return description

townCenterChoices = readDescription("townCenterChoices")

barChoices = readDescription("barChoices")

shopChoices = readDescription("shopChoices")

templeChoices = readDescription("templeChoices")

inTempleChoices = readDescription("inTempleChoices")

schoolOfThought = readDescription("schoolOfThought")

attack1 = readDescription("attack1")

attack2 = readDescription("attack2")

giveDrink1 = readDescription("giveDrink1")

giveDrink2 = readDescription("giveDrink2")

drunk = readDescription("drunk")

runaway = readDescription("runaway")

palaceChoices = readDescription("palaceChoices")

skyrim = readDescription("skyrim")

drifterChoices = readDescription("drifterChoices")

pacifist = readDescription("pacifist")

attackMonksChoices = readDescription("attackMonksChoices")

limbo = readDescription("limbo")

negotiate = readDescription("negotiate")

hesitate = readDescription("hesitate")

#PLAYER STARTING SETUP------------------------------------------------------------------------------

playerPosition = "townCenter"

hasGun = False

hasPOI = False

hasSON = False

playerDrinks = 0

#ROOMS----------------------------------------------------------------------------------------------

def positionPicker (position):
    if position == "townCenter":
        townCenter()
    if position == "bar":
        bar()
    if position == "shop":
        shop()
    if position == "attack":
        attack()
    if position == "temple":
        temple()
    if position == "giveDrink":
        giveDrink()
    if position == "drink":
        drink()
    if position == "inTemple":
        inTemple()
    if position == "palace":
        palace()
    if position == "talkToDrifter":
        talkToDrifter()
    if position == "attackMonks":
        attackMonks()
        
def townCenter():
    print(townCenterChoices)

    choice = int(input("Enter your choice: "))

    print("\n--------------------------------------------------------\n")

    if choice == 1:
        playerPosition = "bar"
        positionPicker(playerPosition)
    elif choice == 2:
        playerPosition = "shop"
        positionPicker(playerPosition)
    elif choice == 3:
        playerPosition = "palace"
        positionPicker(playerPosition)
    elif choice == 4:
        playerPosition = "temple"
        positionPicker(playerPosition)
    else:
        print ("Invalid input.")
        townCenter()

def bar():
    print (barChoices)

    choice = int(input("Enter your choice: "))

    print("\n--------------------------------------------------------\n")

    if choice == 1:
        playerPosition = "attack"
        positionPicker(playerPosition)
    elif choice == 2:
        playerPosition = "giveDrink"
        positionPicker(playerPosition)
    elif choice == 3:
        playerPosition = "drink"
        positionPicker(playerPosition)
    elif choice == 4:
        playerPosition = "townCenter"
        positionPicker(playerPosition)
    else:
        print ("Invalid input.")
        bar()

def shop():
    print (shopChoices)

    choice = int(input("Enter your choice: "))

    print("\n--------------------------------------------------------\n")

    if choice == 1:
        global hasPOI
        hasPOI = True
        print("Purchased.")
        shop()
    elif choice == 2:
        global hasGun
        hasGun = True
        print("Purchased.")
        shop()
    elif choice == 3:
        playerPosition = "townCenter"
        positionPicker(playerPosition)
    else:
        print ("Invalid input.")
        shop()

def temple():
    print (templeChoices)

    choice = int(input("Enter your choice: "))

    print("\n--------------------------------------------------------\n")

    if choice == 1:
        playerPosition = "inTemple"
        positionPicker(playerPosition)
    elif choice == 2:
        print("You ask a nearby monk about the strange plant.\n"
              "Monk: \"Be careful. It's called \'Succulent Of Necrosis\'.\n"
              "I wouldn't eat it.\"\n\n")
        temple()
    elif choice == 3:
        global hasSON
        hasSON = True
        print("You picked a blade of the plant.\n")
        temple()
    elif choice == 4:
        playerPosition = "townCenter"
        positionPicker(playerPosition)
    else:
        print ("Invalid input.")
        temple()

def attack():
    if (hasGun == True):
        print(attack2)
    else:
        print(attack1)

def giveDrink():
    if (hasPOI and hasSON):
        print(giveDrink2)
    else:
        print(giveDrink1)
        bar()

def drink():
    global playerDrinks
    playerDrinks += 1
    print("You enjoy a nice cold brew on your employer's dime.\n\n")
    
    if (playerDrinks > 4):
        print(drunk)
    else:
        bar()

def inTemple():
    print(inTempleChoices)

    choice = int(input("Enter your choice: "))

    print("\n--------------------------------------------------------\n")

    if choice == 1:
        print(runaway)
    elif choice == 2:
        print(schoolOfThought)
        inTemple()
    elif choice == 3:
        playerPosition = "attackMonks"
        positionPicker("attackMonks")
    elif choice == 4:
        playerPosition = "temple"
        positionPicker(playerPosition)
    else:
        print ("Invalid input.")
        inTemple()

def attackMonks():
    print(attackMonksChoices)

    choice = int(input("Enter your choice: "))

    print("\n--------------------------------------------------------\n")

    if choice == 1:
        print(limbo)
    elif choice == 2:
        print(negotiate)
    elif choice == 3:
        print(hesitate)
    else:
        print ("Invalid input.")
        attackMonks()

def palace():
    print(palaceChoices)

    choice = int(input("Enter your choice: "))

    print("\n--------------------------------------------------------\n")

    if choice == 1:
        print("The royal guards easily fend you off and retaliate, killing you instantly.\n"
              "YOU DIED")
    elif choice == 2:
        print(skyrim)
    elif choice == 3:
        playerPosition = "talkToDrifter"
        positionPicker(playerPosition)
    elif choice == 4:
        playerPosition = "townCenter"
        positionPicker(playerPosition)
    else:
        print ("Invalid input.")
        palace()

def talkToDrifter():
    print(drifterChoices)

    choice = int(input("Enter your choice: "))

    print("\n--------------------------------------------------------\n")

    if choice == 1:
        print("Drifter: \"I'm thirsty. And magical. But mostly thirsty.\"\n\n")
        talkToDrifter()
    elif choice == 2:
        global hasPOI
        if hasPOI:
            print(pacifist)
        else:
            print("The drifter thanks you for the spare change. He gives you a coy smile.\n\n")
            talkToDrifter()
    elif choice == 3:
        playerPosition = "palace"
        positionPicker(playerPosition)
    else:
        print ("Invalid input.")
        talkToDrifter()

#START----------------------------------------------------------------------------------------------

positionPicker(playerPosition)


    




