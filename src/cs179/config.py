# config.py
from pathlib import Path

try:
    DATA_DIR = Path(__file__).parent.parent.parent / 'data'
except Exception:
    DATA_DIR = Path.home() / 'teach' / 'cs179' / 'data'
PRIVATE_DIR = DATA_DIR / 'private'
CANVAS_API_URL = 'https://sdccd.instructure.com'
CANVAS_COURSE_ID = 2450635
CANVAS_COURSE_URL = f'https://sdccd.instructure.com/courses/{CANVAS_COURSE_ID}'
